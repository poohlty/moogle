open WorldObject
open WorldObjectI
open Hive
open Bear

(* ### Part 6 Custom Events ### *)
let spawn_bear_pollen = 300

(** A cave will spawn a bear when the hive has collected a certain amount of
    honey. *)
class cave p hive: world_object_i =
object (self)
  inherit world_object p as super

  val mutable bear_in_cave = true

  initializer
    self#register_handler hive#get_pollen_event (fun x -> self#do_action x);

  method private do_action x =
    if (x > spawn_bear_pollen) && bear_in_cave then 
    (let my_bear = new bear (self#get_pos) hive self in
      self#register_handler my_bear#get_die_event (fun _ -> bear_in_cave <- true);
      bear_in_cave <- false;())

  method receive_pollen lst = []

  (******************************)
  (***** Instance Variables *****)
  (******************************)

  (* ### TODO: Part 6 Custom Events ### *)

  (***********************)
  (***** Initializer *****)
  (***********************)

  (* ### TODO: Part 6 Custom Events ### *)

  (**************************)
  (***** Event Handlers *****)
  (**************************)

  (* ### TODO: Part 6 Custom Events ### *)

  (********************************)
  (***** WorldObjectI Methods *****)
  (********************************)

  (* ### TODO: Part 1 Basic ### *)

  method get_name = "cave"

  method draw = self#draw_circle Graphics.black Graphics.white "C"

  method draw_z_axis = 1


  (* ### TODO: Part 6 Custom Events *)

end
