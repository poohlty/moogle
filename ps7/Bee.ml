open WorldObject
open WorldObjectI
open Movable
open World
open Ageable
open CarbonBased

(* ### Part 2 Movement ### *)
let bee_inverse_speed = Some 1

(* ### Part 3 Actions ### *)
let max_pollen_types = 5

(* ### Part 4 Aging ### *)
let bee_lifetime = 1000

(* ### Part 5 Smart Bees ### *)
let max_sensing_range = 5

(** Bees travel the world searching for honey.  They are able to sense flowers
    within close range, and they will return to the hive once they have
    pollenated enough species of flowers. *)
class type bee_t =
object
  inherit Ageable.ageable_t

  method private next_direction_default : Direction.direction option
end
class bee p (home:world_object_i) : bee_t =

object (self)
  inherit carbon_based p bee_inverse_speed (World.rand bee_lifetime) bee_lifetime as super

  (******************************)
  (***** Instance Variables *****)
  (******************************)

  val mutable pollen : int list = []

  val mutable danger : bool = false

  val mutable danger_obj : world_object_i = home

  val sensing_range = World.rand max_sensing_range

  val pollen_types = World.rand max_pollen_types + 1
  (* ### TODO: Part 3 Actions ### *)
  
  (* ### TODO: Part 5 Smart Bees ### *)

  (* ### TODO: Part 6 Custom Events ### *)

  (***********************)
  (***** Initializer *****)
  (***********************)

  initializer
    self#register_handler World.action_event (fun _ -> self#do_action);
    self#register_handler home#get_danger_event 
    (fun x -> danger <- true;
    self#register_handler x#get_die_event (fun _ -> danger <- false);
    danger_obj <- x);
  (* ### TODO: Part 3 Actions ### *)
  
  method private do_action = 
    (List.iter self#deposit_pollen (World.get (self#get_pos));
    List.iter self#extract_pollen (World.get (self#get_pos)));
    if danger && (danger_obj#get_pos = self#get_pos) then 
      ((danger_obj#receive_sting);(self#die))

  (* ### TODO: Part 6 Custom Events ### *)

  (**************************)
  (***** Event Handlers *****)
  (**************************)

  (* ### TODO: Part 6 Custom Events ### *)

  (**************************)
  (***** Helper Methods *****)
  (**************************)
  method private extract_pollen obj = 
    match obj#forfeit_pollen with
     | None -> ()
     | Some p -> pollen <- p::pollen 
  
  method private deposit_pollen obj = 
    pollen <- (obj#receive_pollen pollen)
  
  method private filter_flower x =
    match x#smells_like_pollen with
     | None -> false
     | Some(s) -> not(List.mem s pollen)

  method private find_closest lst =
    match lst with
    | [] -> None
    | _ -> let hd::tl = List.sort 
    (fun x y -> compare (Direction.distance self#get_pos x#get_pos) 
                        (Direction.distance self#get_pos y#get_pos)) lst
    in Some(hd)

  method private magnet_flower : world_object_i option =
    let f = World.objects_within_range self#get_pos sensing_range in
    let f2 = List.filter self#filter_flower f in
    self#find_closest f2

  (* ### TODO: Part 3 Actions ### *)

  (* ### TODO: Part 5 Smart Bees ### *)

  (********************************)
  (***** WorldObjectI Methods *****)
  (********************************)

  (* ### TODO: Part 1 Basic ### *)

  method get_name = "bee"

  (* ### TODO: Part 4 Aging ### *)
  method draw_picture = 
    self#draw_circle Graphics.yellow Graphics.black (string_of_int(List.length pollen))

  method draw_z_axis = 2


  (* ### TODO: Part 3 Actions ### *)

  (***************************)
  (***** Movable Methods *****)
  (***************************)

  (* ### TODO: Part 2 Movement ### *)

  method private next_direction_default = 
    Some(Direction.random World.rand)

  method next_direction =
    if danger then
      (World.direction_from_to self#get_pos danger_obj#get_pos)
    else
      if List.length (Helpers.unique pollen) > pollen_types
      then (World.direction_from_to self#get_pos home#get_pos)
      else match self#magnet_flower with
      | Some(f) -> (World.direction_from_to self#get_pos f#get_pos)
      | None -> self#next_direction_default

  (* ### TODO: Part 5 Smart Bees ### *)

  (* ### TODO: Part 6 Custom Events ### *)

  (***********************)
  (***** Bee Methods *****)
  (***********************)

  (* ### TODO: Part 5 Smart Bees ### *)

end
