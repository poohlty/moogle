open WorldObject
open WorldObjectI
open Movable
open Helpers

(* ### Part 2 Movement ### *)
let cow_inverse_speed = Some 1

(* ### Part 6 Custom Events ### *)
let max_consumed_objects = 100

(** Cows will graze across the field until it has consumed a satisfactory number
    of flowers *)
class cow p hive home: movable =
object (self)
  inherit movable p cow_inverse_speed as super

  val mutable eaten_num : int = 0

  val mutable hungery : bool = true

  (******************************)
  (***** Instance Variables *****)
  (******************************)

  (* ### TODO: Part 3 Actions ### *)

  (***********************)
  (***** Initializer *****)
  (***********************)
  
  initializer
    self#register_handler World.action_event (fun _ -> self#do_action)
  
  (* ### TODO: Part 3 Actions ### *)

  method private eat obj =
    if (obj#smells_like_pollen != None) && (eaten_num < max_consumed_objects) then 
      (obj#die; eaten_num <- eaten_num + 1; flush_all(print_string "*nom* "))
    else ()
    
  method private do_action = 
    if (self#get_pos = home#get_pos) && (eaten_num >= max_consumed_objects) then self#die
    else (List.iter self#eat (World.get (self#get_pos)))

  (**************************)
  (***** Event Handlers *****)
  (**************************)

  (* ### TODO: Part 3 Actions ### *)

  (* ### TODO: Part 6 Custom Events ### *)

  (********************************)
  (***** WorldObjectI Methods *****)
  (********************************)

  (* ### TODO: Part 1 Basic ### *)

  method get_name = "cow"

  method draw = 
    self#draw_circle (Graphics.rgb 180 140 255) Graphics.black (string_of_int(eaten_num))

  method draw_z_axis = 4


  (* ### TODO: Part 3 Actions ### *)

  (***************************)
  (***** Movable Methods *****)
  (***************************)

  (* ### TODO: Part 2 Movement ### *)

  method next_direction = 
    if eaten_num < max_consumed_objects then
    Helpers.with_inv_probability_or World.rand (World.size/2) 
      (fun () -> (World.direction_from_to self#get_pos hive#get_pos))
      (fun () -> Some(Direction.random World.rand))
    else 
      (World.direction_from_to self#get_pos home#get_pos)



  (* ### TODO: Part 6 Custom Events ### *)

end
