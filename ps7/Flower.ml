open WorldObject
open WorldObjectI
open Helpers
open Ageable
open CarbonBased

(* ### Part 3 Actions ### *)
let next_pollen_id = ref 0
let get_next_pollen_id () =
  let p = !next_pollen_id in incr next_pollen_id ; p

(* ### Part 3 Actions ### *)
let max_pollen = 5
let produce_pollen_probability = 50
let bloom_probability = 4000
let forfeit_pollen_probability = 3

(* ### Part 4 Aging ### *)
let flower_lifetime = 2000

(** Flowers produce pollen.  They will also eventually die if they are not cross
    pollenated. *)
class flower p pollen_id: ageable_t =
object (self)
  inherit carbon_based p None (World.rand flower_lifetime) flower_lifetime as super

  (******************************)
  (***** Instance Variables *****)
  (******************************)
  val mutable pollen : int list = 
    Helpers.replicate (World.rand max_pollen) pollen_id

  (* ### TODO: Part 3 Actions ### *)

  (***********************)
  (***** Initializer *****)
  (***********************)

  initializer
    self#register_handler World.action_event (fun _ -> self#do_action)
  (* ### TODO: Part 3 Actions ### *)
  
  method private do_action = 
    (Helpers.with_inv_probability World.rand produce_pollen_probability self#produce_pollen;
    Helpers.with_inv_probability World.rand bloom_probability self#bloom)
    
  (**************************)
  (***** Event Handlers *****)
  (**************************)

  (* ### TODO: Part 3 Actions ### *)

  method private produce_pollen () = 
   if (List.length pollen) < max_pollen then (pollen <- pollen_id::pollen)
    else ()

  method private bloom () =
    World.spawn 1 self#get_pos (fun tp -> new flower tp pollen_id;())

  method smells_like_pollen =
    if (List.length pollen = 0) then None else Some(pollen_id)

  method forfeit_pollen = 
    Helpers.with_inv_probability_or World.rand forfeit_pollen_probability 
    (fun () -> 
      match pollen with
      | [] -> None
      | hd::tl -> (pollen <- tl;Some(pollen_id)))
    (fun () -> None)

  method receive_pollen lst =
    if (List.fold_right (fun x y -> (x = pollen_id) && y) (Helpers.unique lst) true)
    then (self#reset_life;lst)
    else lst

  (********************************)
  (***** WorldObjectI Methods *****)
  (********************************)

  (* ### TODO: Part 1 Basic ### *)

  method get_name = "flower"

  (* ### TODO: Part 4 Aging ### *)
  method draw_picture = 
    self#draw_circle (Graphics.rgb 255 150 255) Graphics.black (string_of_int(List.length pollen))

  method draw_z_axis = 1

  (* ### TODO: Part 3 Actions ### *)


  (***************************)
  (***** Ageable Methods *****)
  (***************************)

  (* ### TODO: Part 4 Aging ### *)

end
