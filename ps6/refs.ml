(* Consider this mutable list type. *)
type 'a mlist = Nil | Cons of 'a * (('a mlist) ref)

(*>* Problem 1.1 *>*)
(* Write a function has_cycle that returns whether a mutable list has a cycle.
 * You may want a recursive helper function. Don't worry about space usage. *)

let rec cyclic_helper mlst lst n=
	match mlst with
	| Nil -> (None, n)
	| Cons (_, t) -> 
		match !t with
		| Nil -> (None, n+1)
		| Cons (_, { contents = Nil }) -> (None, n+2)
		| Cons (_, t2) -> 				
		if List.exists (fun x -> x == t2) lst then (Some t, n+1)
		else cyclic_helper !t (t::lst) (n+1)
;;

let has_cycle (lst : 'a mlist) : bool =
	match (cyclic_helper lst [] 0) with
	| (None, _) -> false
	| _ -> true
;;

(* Some mutable lists for testing. *)
let list0 = Cons(2,ref Nil);;

let list1a = Cons(2, ref Nil)
let list1b = Cons(2, ref list1a)
let list1 = Cons(1, ref list1b);;

let reflist = ref (Cons(2, ref Nil))
let list2 = Cons(1, ref (Cons (2, reflist)))
let _ = reflist := list2;;

let reflist2 = ref (Cons(2, ref Nil))
let list3 = Cons(1, ref (Cons (2, ref(Cons (3, reflist2)))))
let _ = reflist2 := list3;;

assert (has_cycle list0 = false);;
assert (has_cycle list1 = false);;
assert (has_cycle list2 = true);;
assert (has_cycle list3 = true);;

(*>* Problem 1.2 *>*)
(* Write a function flatten that flattens a list (removes its cycles if it
 * has any) destructively. Again, you may want a recursive helper function and
 * you shouldn't worry about space. *)
let flatten (lst : 'a mlist) : unit =
	match cyclic_helper lst [] 0 with
	| (None, _) -> ()
	| (Some t, _) -> (t := Nil)
;;

(*>* Problem 1.3 *>*)
(* Use write mlength, which finds the number of nodes in a mutable list. *)

let mlength (lst : 'a mlist) : int =
	let (o,n) = cyclic_helper lst [] 0 in n
;;

assert (mlength list0 = 1);;
assert (mlength list1 = 3);;
assert (mlength list2 = 2);;
assert (mlength list3 = 3);;

assert (flatten list0; list0 = Cons (2, {contents = Nil}));;
assert (flatten list1; list1 = Cons (1, {contents = Cons (2, {contents = Cons (2, {contents = Nil})})}));;
assert (flatten list2; list2 = Cons (1, {contents = Cons (2, {contents = Nil})}));;
assert (flatten list3; list3 = Cons (1, {contents = Cons (2, {contents = Cons (3, {contents = Nil})})}));;

(*>* Problem 1.4 *>*)
(* Please give us an honest estimate of how long this part took 
 * you to complete.  We care about your responses and will use
 * them to help guide us in creating future assignments. *)
let minutes_spent : int = 40
