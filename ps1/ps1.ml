(*** CS 51 Problem Set 1 ***)
(*** Tianyu Liu ***)

(* Problem 1 - Fill in types:
 * Replace each ??? with the appropriate type of the corresponding expression.
 * Be sure to remove the comments from each subproblem and to type check it
 * before submission. *)

(*>* Problem 1a *>*)


let prob1a : string  = let greet y = "Hello " ^ y in greet "World!" ;;


(*>* Problem 1b *>*)

let prob1b : int option list = [Some 4; Some 2; None; Some 3] ;;


(*>* Problem 1c *>*)

let prob1c : (float option * float option) * bool  = ((None, Some 42.0), true) ;;


(* Explain in a comment why the following will not type check,
   and provide a fix *)

(*>* Problem 1d *>*)

let prob1d : (string * int) list = [("CS", 51); ("CS", 50)] ;;

(*
the return type is string * int list but the expression is a list. add () to the elements to change them to tuples
*)

(*>* Problem 1e *>*)

let prob1e : int =
  let compare (x,y) = x < y in
  if compare (4.0, 3.9) then 4 else 2;;

(*
pass in two different datatypes to function compare, change 4 to 4.0 to fix the issue
*)

(*>* Problem 1f *>*)

let prob1f : (string * int option) list =
  [("January", None); ("February", Some 1); ("March", None); ("April", None);
   ("May", None); ("June", Some 1); ("July", None); ("August", None);
   ("September", Some 3); ("October", Some 1); ("November", Some 2); ("December", Some 3)] ;;

(*The returned type contains (string * string) pair but in the expression the second argument in the pair is an int option. change 
the second string into (int) option
*)


(* Problem 2 - Fill in expressions to satisfy the following types:
 *
 * NOTE: for option and list types, you must go at least one layer deep.
 * example problems:
 *   let x : int option = Some 7 ;;
 *   let y : int list = ??? ;;
 * incorrect answers:
 *   let x : int option = None ;;
 *   let y : int list = [] ;;
 * possible correct answers:
 *   let x : int option = Some 1 ;;
 *   let y : int list = [1] ;;
 *   let y : int list = [1; 2] ;;
 *   let y : int list = 1 :: [] ;;
 *)

(*>* Problem 2a *>*)

let prob2a : (int * (string * float) option list) list = 
  [7, [Some ("Tianyu",3.5)]]
;;


(*>* Problem 2b *>*)

type student = {name: string; year: int option; house: string option};;
let prob2b : (student list option * int) list =
  [Some [{name="Tianyu"; year= Some(2016); house=Some("Straus")}], 7]
;;


(*>* Problem 2c *>*)

let prob2c : (float * float -> float) * (int -> int -> unit) * bool  =
  let foo (x:(float * float)) : float = 3.5 in
  let bar (x:int) (y:int) = () in
  (foo, bar, true)
;;


(* Fill in ??? with something that makes these typecheck: *)
(*>* Problem 2d *>*)

let prob2d =
  let rec foo bar =
    match bar with
    | (a, (b, c)) :: xs -> if a then (b - c + (foo xs)) else foo xs
    | _ -> 0
  in foo [(true,(2,3))]
;;

(*>* Problem 2e *>*)

let prob2e =
  let v = (32.0, 28.0) in
  let square x = x *. x in
  let squared_norm (w: float * float) : float = 
    let (a,b) = w in
    square a +. square b in
  let d = sqrt (squared_norm v) in
  int_of_float d
;;


(* Problem 3 - Write the following functions *)
(* For each subproblem, you must implement a given function and corresponding
 * unit tests.  You are provided a high level description as well as a
 * prototype of the function you must implement. *)

(*>* Problem 3a *>*)

(* reversed lst should return [true] if the integers in the list are in
 * decreasing order. The empty list is considered to be reversed. *)

(* Here is its prototype/signature: *)
(* reversed : int list -> bool *)

(* Implement reversed below, and be sure to write tests for it (see 3b for
 * examples of tests). *)

let rec reversed (input:int list): bool = 
  match input with
    | hd1::hd2::tl -> if hd1 > hd2 then reversed (hd2::tl) else false
    | _ -> true
;;

assert ((reversed [1;2;3]) = false);;
assert ((reversed [3;2;1]) = true);;
assert ((reversed []) = true);;

(*>* Problem 3b *>*)

(* merge xs ys takes two integer lists, each sorted in increasing order,
 and returns a single merged list in sorted order.  For example:
# merge [1;3;5] [2;4;6];;
- : int list = [1; 2; 3; 4; 5; 6]
# merge [1;3;5] [2;4;6;12];;
- : int list = [1; 2; 3; 4; 5; 6; 12]
# merge [1;3;5;700;702] [2;4;6;12];;
- : int list = [1; 2; 3; 4; 5; 6; 12; 700; 702]
*)

(* The type signature for [merge] is as follows: *)
(* merge : int list -> int list -> int list *)

let rec merge (xs:int list) (ys:int list): int list =
  match (xs,ys) with
    |([],[]) -> []
    |(xhd::xtl,[]) -> xhd::xtl
    |([],yhd::ytl) -> yhd::ytl
    |(xhd::xtl,yhd::ytl) -> if xhd < yhd then xhd::merge xtl (yhd::ytl) else yhd::merge (xhd::xtl) ytl
;;


(* sample tests *)
assert ((merge [1;2;3] [4;5;6;7]) = [1;2;3;4;5;6;7]) ;;
assert ((merge [4;5;6;7] [1;2;3]) = [1;2;3;4;5;6;7]) ;;
assert ((merge [4;5;6;7] [1;2;3]) = [1;2;3;4;5;6;7]) ;;
assert ((merge [2;2;2;2] [1;2;3]) = [1;2;2;2;2;2;3]) ;;
assert ((merge [1;2] [1;2]) = [1;1;2;2]) ;;
assert ((merge [-1;2;3;100] [-1;5;1001]) = [-1;-1;2;3;5;100;1001]) ;;
assert ((merge [] []) = []) ;;
assert ((merge [1] []) = [1]) ;;
assert ((merge [] [-1]) = [-1]) ;;
assert ((merge [1] [-1]) = [-1;1]) ;;

(*>* Problem 3c *>*)
(* unzip should be a function which, given a list of pairs, returns a
   pair of lists, the first of which contains each first element of
   each pair, and the second of which contains each second element.
   The returned lists should have the elements in the order that they
   appeared in the input.  So, for instance,
   unzip [(1,2);(3,4);(5,6)] = ([1;3;5],[2;4;6])
*)

(* The type signature for unzip is as follows: *)
(* unzip : (int * int) list -> int list * int list) *)

let rec unzip (x:(int * int) list) : int list * int list =
  match x with 
  |[] -> ([],[])
  |(a,b)::tl -> let (c,d) = unzip tl in (a::c,b::d)
;;

assert ((unzip []) = ([],[]));;
assert ((unzip [(1,2);(3,4);(5,6)]) = ([1;3;5],[2;4;6]));;
assert ((unzip [(1,2)]) = ([1],[2]));;


(*>* Problem 3d *>*)

(* variance lst should be a function that returns None if lst has fewer than 2
floats, and Some of the variance of the floats in lst otherwise.  Recall that
the variance of a sequence of numbers is 1/(n-1) * sum (x_i-m)^2, where
 a^2 means a squared, and m is the arithmetic mean of the list (sum of list /
length of list).  For example,
 - variance [1.0; 2.0; 3.0; 4.0; 5.0] = Some 2.5
 - variance [1.0] = None

Remember to use the floating point version of the arithmetic operators when
operating on floats (+. *., etc).  The "float" function can cast an int to a
float.
*)

(* variance : float list -> float option *)

  
let variance (x: float list): float option =
  (* square a float *)
  let square x = x *. x in
  (* sum up a list of float *)
  let rec sum (s: float list) : float = 
    match s with
      | [] -> 0.0
      | hd::tl -> hd +. sum tl in
  (* calculate the length of a given list *)
  let rec length (l: float list) : float = 
    match l with 
      | [] -> 0.0
      | hd::tl -> 1.0 +. (length tl) in 
  (* convert a float list into a list of variance of each element*)
  let rec mean (m:float list) : float list = 
    match m with
      | [] -> []
      | hd::tl -> square((hd -. (sum x /. length x)))::(mean tl) in
  (* main variance logic*)
  match x with
    | hd1::hd2::tl  -> Some (1.0 /. (length x -. 1.0) *. sum (mean x))
    | _ -> None
;;

assert ((variance []) = None);; 
assert ((variance [1.0]) = None);;
assert ((variance [1.0;1.0]) = Some 0.0);;
assert ((variance [1.0;2.0;3.0;4.0;5.0]) = Some 2.5);;


(*>* Problem 3e *>*)

(* few_divisors n m should return true if n has fewer than m divisors,
 * (including 1 and n) and false otherwise. Note that this is *not* the
 * same as n having fewer divisors than m:
few_divisors 17 3;;
- : bool = true
# few_divisors 4 3;;
- : bool = false
# few_divisors 4 4;;
- : bool = true
 *)
(* The type signature for few_divisors is: *)
(* few_divisors : int -> int -> bool *)


let few_divisors (n:int) (m:int): bool =
  (* calculate the number of divisors of a given number *)
  let rec divisor (a:int) (b:int) : int = 
    if a = b  then 1 else
    if a mod b = 0 then 1 + divisor a (b+1) else divisor a (b+1) in 
  (* few_divisors logid *)
  if n = 0 then false else 
  if divisor n 1 < m then true else false
;;

assert ((few_divisors 2 1) = false) ;;
assert ((few_divisors 5 4) = true) ;;
assert ((few_divisors 5 3) = true) ;;
assert ((few_divisors 0 1) = false) ;;


(*>* Problem 3f *>*)

(* concat_list sep lst should take a list of strings and return one big
   string with all the string elements of lst concatenated together, but
   separated by the string sep.  Here are some example tests:
concat_list ", " ["Greg"; "Anna"; "David"] ;;
- : string = "Greg, Anna, David"
concat_list "..." ["Moo"; "Baaa"; "Quack"] ;;
- : string = "Moo...Baaa...Quack"
concat_list ", " [] ;;
- : string = "" ;;
concat_list ", " ["Moo"] ;;
- : string = "Moo"
*)
(* The type signature for concat_list is: *)
(* concat_list : string -> string list -> string *)

let rec concat_list (sep:string) (x: string list): string =
   match x with
   |[] -> ""
   |[a] -> a
   |a::tl -> a ^ sep ^ concat_list sep tl
;;

assert ((concat_list ", " []) = "") ;;
assert ((concat_list ", " ["Greg"; "Anna"; "David"]) = "Greg, Anna, David") ;;
assert ((concat_list "..." ["Moo"; "Baaa"; "Quack"]) = "Moo...Baaa...Quack") ;;
assert ((concat_list ", " ["Moo"]) = "Moo") ;;

(*>* Problem 3g *>*)

(* One way to compress a list of characters is to use run-length encoding.
  The basic idea is that whenever we have repeated characters in a list
  such as ['a';'a';'a';'a';'a';'b';'b';'b';'c';'d';'d';'d';'d'] we can
  (sometimes) represent the same information more compactly as a list
  of pairs like [(5,'a');(3,'b');(1,'c');(4,'d')].  Here, the numbers
  represent how many times the character is repeated.  For example,
  the first character in the string is 'a' and it is repeated 5 times,
  followed by 3 occurrences of the character 'b', followed by one 'c',
  and finally 4 copies of 'd'.

  Write a function to_run_length that converts a list of characters into
  the run-length encoding, and then write a function from_run_length
  that converts back.  Writing both functions will make it easier to
  test that you've gotten them right.
*)
(* The type signatures for to_run_length and from_run_length are: *)
(* to_run_length : char list -> (int * char) list *)
(* from_run_length : (int * char) list -> char list *)

let rec to_run_length (x:char list) : (int * char) list = 
    match x with
    |[] -> []
    |hd::tl ->
    (* counts the number of the first group of consecutive letters in a given list *)
    let rec count (x:char list) : int = 
       match x with
       |hd1::hd2::tl -> if hd1 != hd2 then 1 else 1 + count (hd2::tl)
       |hd::tl -> 1
       |[] -> 0 in
    (* cut the first group of consecutive letters in a given list *)
    let rec cut (x:char list) : char list =
       match x with
       |hd1::hd2::tl -> if hd1 != hd2 then hd2::tl else cut (hd2::tl)
       |_ -> [] in
    (* main to_run_length logic *)
    (count x, hd)::to_run_length (cut x)
;;

assert ((to_run_length []) = []);;
assert ((to_run_length ['a';'a';'a';'b']) = [(3, 'a'); (1, 'b')]);;
assert ((to_run_length ['a';'a';'a';'a';'a';'b';'b';'b';'c';'d';'d';'d';'d']) = [(5,'a');(3,'b');(1,'c');(4,'d')]);;


let rec from_run_length (x:(int * char) list) : char list =
    match x with
    |[] -> []
    |(a,b)::tl -> if a > 0 then b::from_run_length ((a-1,b)::tl) else from_run_length tl
;;

assert ((from_run_length []) = []);;
assert ((from_run_length [(2,'a');(1,'b')]) = ['a';'a';'b']);;

(*>* Problem 4 *>*)

(* Challenge!

  permutations lst should return a list containing every
  permutation of lst.  For example, one correct answer to
  permutations [1; 2; 3] is
  [[1; 2; 3]; [2; 1; 3]; [2; 3; 1]; [1; 3; 2];	[3; 1; 2]; [3; 2; 1]].

  It doesn't matter what order the permutations appear in the returned list.
  Note that if the input list is of length n then the answer should be of
  length n!.

  Hint:
  One way to do this is to write an auxiliary function,
  interleave : int -> int list -> int list list,
  that yields all interleavings of its first argument into its second:
  interleave 1 [2;3] = [ [1;2;3]; [2;1;3]; [2;3;1] ].
  You may also find occasion for the library functions
  List.map and List.concat.
*)

(* The type signature for permuations is: *)
(* permutations : int list -> int list list *)

let rec permutation (x: int list) : (int list) list =
  match x with
    |[] -> [[]]
    |hd::tl ->  
     (* return all the possible positions of an int in a given list *)
      let rec interleave (x:int) (y:int list) : (int list) list = 
        match y with 
          |[] -> [[x]]
          |hd::tl ->
          (* append an int to a list, used appending int to returning interleave lists *)
          let append x y = x::y in
            (x::y)::(List.map (append hd) (interleave x tl)) in
      List.concat (List.map (interleave hd) (permutation tl))
;;

assert ((permutation []) = [[]]);;
assert ((permutation [1]) = [[1]]);;
assert ((permutation [1;2;3]) = [[1; 2; 3]; [2; 1; 3]; [2; 3; 1]; [1; 3; 2]; [3; 1; 2]; [3; 2; 1]]);;

